<?php

namespace Accede\Behaviors\Bootstrapable;
use Propel\Generator\Model\Behavior;

class BootstrapableBehavior extends Behavior {
	// default parameters value
	protected $parameters = array('hideid' => true, );

	public function objectMethods() {
		$script = "";
		$script .= $this -> addTableView();
		$script .= $this -> addColView();
		return $script;
	}

	protected function addTableView() {

		$table = $this -> getTable();

		if ($table -> hasCompositePrimaryKey()) {
			throw new \Exception('Bootstrable currently donst work with CompositePrimaryKeys (Table: '.$table->getName().')');
		}
		if (!$table -> hasPrimaryKey()) {
			throw new \Exception('Bootstrable requires a PrimaryKey');
		}

		$hasslug = true;
		//todo add this check

		$cols = array();

		foreach ($table->getColumns() as $col) {
			if (!($col -> isPrimaryKey() and ($this -> parameters['hideid']))) {
				if (!(($col -> getName() == "slug") and $hasslug)) {
					$cols[$col -> getName()] = $col;
				}
			}

		}

		$primarykey = $table -> getPrimaryKey()[0];

		return $this -> renderTemplate('tableView', array('table' => $table, 'cols' => $cols, 'pk' => $primarykey, 'hasslug' => $hasslug));

	}

	protected function addColView() {

		$table = $this -> getTable();

		if ($table -> hasCompositePrimaryKey()) {
			throw new \Exception('Bootstrable currently donst work with CompositePrimaryKeys');
		}
		if (!$table -> hasPrimaryKey()) {
			throw new \Exception('Bootstrable requires a PrimaryKey');
		}

		$hasslug = true;
		//todo add this check

		$render = "";

		foreach ($table->getColumns() as $col) {
			$render .= $this -> renderTemplate('editView', array('col' => $col));

		}

		return $render;

	}

}
