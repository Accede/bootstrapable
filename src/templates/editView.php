
/*  Render a bootstrap field for <?php echo $col->getPhpName(); ?> 
 * 
 */
public function render<?php echo $col->getPhpName(); ?>()
{
	<?php 
	$type= "text";
	?>
  
  $row  = '<div class="form-group">';
  $row .= '<label class="col-md-3 control-label" for="<?php echo $col->getPhpName(); ?>"><?php echo $col->getPhpName(); ?></label>';
  $row .= '<div class="col-md-8">';
  $row .= '<input id="textinput" name="<?php echo $col->getPhpName(); ?>" type="<?php echo $type; ?>" required="required" value="'.$this->get<?php echo $col->getPhpName(); ?>().'" class="form-control input-md">';
  $row .= '</div>';
  $row .= '</div>';
  return $row;
}	


