/**
 * Creates a table view for <?php echo $table->getName() ?>
 *
 * 
 */


public function renderTableRow($ishead = false, $urlroot = "/", $extra = array())
{
  $row = '<tr>';
  if ($ishead) {
<?php
 	foreach ($cols as $name => $col) {
		echo "    \$row .= '<th>$name</th>'; \n";
	}
?>
  } else {
  	<?php 
  	if ($hasslug) {
  	?> $url = $urlroot.$this->getSlug(); <?php
  	} else {
  	?> $url = $urlroot.$this->get<?php echo $pk->getPhpName(); ?>(); <?php	
  	}
	foreach ($cols as $name => $col) {
	

	$getter = "\$this->get".$col->getPhpName()."()";
	
	
	
switch ($col->getType()) {
	case 'TIMESTAMP':
		$getter = "(empty($getter)?'':".$getter.'->format(\'d/m/Y H:i:s\'))';
		break;
	
	default:
		break;
}
 		
		
 		if ($col->isPrimaryString()) {
		echo "    \$row .= '<td><a href=\"'.\$url.'\">'.$getter.'</a></td>'; \n";
			
		} else {
		echo "    \$row .= '<td>'.$getter.'</td>'; \n";
			
		}
	}
?>
  }
  	
  	 foreach ($extra as $value){
  	 	  if ($ishead) {
  	 	  	$row .= "<th>$value</th>"; 
  	 	} else {
  	 		$row .= "<td>$value</td>"; 
  	 		
  	 	}
  	 }
  	
  $row .= '</tr>';
  
  return $row;
}	